<?php

use App\Http\Controllers\ExpedienteController;
use App\Http\Controllers\StudentLogController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('expediente', [App\Http\Controllers\HomeController::class,'expediente'])->name('expedientes.expediente');

Route::get('home/{id}', [App\Http\Controllers\HomeController::class,'buscar'])->name('expedientes.buscar');

Route::post('store', [App\Http\Controllers\HomeController::class,'store'])->name('expedientes.store');

Route::post('buscaID', [App\Http\Controllers\HomeController::class,'buscaID'])->name('expedientes.buscaID');

Route::post('eliminar/{estudianteBusca}', [App\Http\Controllers\HomeController::class,'destroy'])->name('expedientes.destroy');

Route::post('limpiar/{estudianteBusca}', [App\Http\Controllers\HomeController::class,'limpiar'])->name('expedientes.limpiar');

Route::post('actualiza/{estudianteBusca}', [App\Http\Controllers\HomeController::class,'actualiza'])->name('expedientes.actualiza');

