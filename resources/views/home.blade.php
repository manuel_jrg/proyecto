@extends('layouts.app')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<style>
    div .select{
        width: 170px;
    }
    
    div h6{
        font-size: 15px;
    }
    div h5{
        font-size: 18px;
    }
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header alert alert-success"><h6><strong>{{ __('Dashboard') }}</strong></h6></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (isset( $dat))
                        @if ($dat == 0)
                            <div class="alert alert-danger" role="alert">
                                <h5 class="text-danger">ID introducido no existe en registros.</h5>
                            </div>
                        @elseif($dat == 1)
                            <div class="alert alert-success" role="alert">
                                <h5 class="text-success">Estudiante registrado con exito.</h5>
                            </div>
                        @elseif($dat == 2)
                            <div class="alert alert-warning" role="alert">
                                <h5 class="text-secondary">Estudiante eliminado con exito.</h5>
                            </div>
                        @else
                            <div class="alert alert-success" role="alert">
                                <h5 class="text-success">Datos del estudiante actualizados.</h5>
                            </div>
                        @endif
                    @endif
                        
                    
                    <div class="row">
                    <div class="col-2  border-top">
                            <div class="row">
                                <div class="w-100"></div>
                                <div class="col text-center pb-1 pt-2"><h5><strong>Expedientes</strong></h5></div>
                                <div class="w-100"></div>

                                <form action="{{route('expedientes.buscaID')}}" method="post">
                                @csrf
                                   <div class="col text-center pb-1 pt-1"><input type="text" name="id" class="form-control" placeholder="Busqueda por ID">
                                <button class="btn btn-primary mt-1" type="submit"><strong>Buscar</strong></button></div>
                                </form>
                    
                                <div class="w-100"></div>
                                <div class="col  border-top pt-2 pb-2" style="overflow-y: scroll; height: 330px;">
                                    @if ($estudianteBusca->id != 0)
                                    <a class="btn btn-info mb-2" href="{{route('expedientes.buscar', $estudianteBusca->id)}}" role="button">{{$estudianteBusca->name.' '.$estudianteBusca->last_name}}</a><br>   
                                    @endif
                                    @foreach ($estudiantes as $estudiante)
                                        @if ($estudianteBusca->id != $estudiante->id)
                                        <h5><a class="btn btn-outline-info" href="{{route('expedientes.buscar', $estudiante->id)}}" role="button">{{$estudiante->name.' '.$estudiante->last_name}}</a><br></h5>    
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    <div class="col-10 ">
                        <form action=" {{route('expedientes.store')}} " method="POST">
                        @csrf
                        <div class="row">
                            <div class="w-100"></div>
                            <div class="col-3 border-top border-left pt-1"><h6 class="pb-3 pt-2"><strong> ID Num:</strong></h6><input type="text" class="form-control" value="{{$estudianteBusca->id}}" disabled style="font-weight: bold;"></div>
                            <div class="col-3 border-top border-left pt-1"><h6 class="pb-3 pt-2"><strong> Cedula:</strong></h6><input required type="text" name="document_id" class="form-control" value="{{$estudianteBusca->document_id}}"></div>
                            
                            <div class="col-6 border-top border-left pt-5 text-right ">
                                @if ($estudianteBusca->id == 0)
                                <button class="btn btn-outline-success botones" type="submit"><strong>New student Log</strong></button>
                                @else
                                <button class="btn btn-outline-success botones" disabled data-toggle="tooltip" data-placement="top" title="Tooltip on top" type="submit"><strong>New student Log</strong></button>
                                <h6 style="font-size: 10px;" class="font-weight-bold text-secondary mt-1">* Para registrar un nuevo estudiante debe limpiar los campos con el botón Clean</h6>
                                @endif
                            </div>
                            <div class="w-100"></div>
                            <div class="col-6 border-top border-left pb-1"><h6 class="pb-3 pt-2"><strong> Fisrt name:</strong></h6><input required type="text" name="name" class="form-control" value="{{$estudianteBusca->name}}"></div>
                            <div class="col-6 border-top border-left pb-1"><h6 class="pb-3 pt-2"><strong> Last name:</strong></h6><input required type="text" name="last_name" class="form-control" value="{{$estudianteBusca->last_name}}"></div>
                            <div class="w-100"></div>
                            <div class="col-4 border-top border-left pb-1 pt-1" ><h6 class="pb-3 pt-2"><strong> Home phone:</strong></h6><input required type="text" name="home_phone"  class="form-control" value="{{$estudianteBusca->home_phone}}"></div>
                            <div class="col-4 border-top border-left pb-1 pt-1"><h6 class="pb-3 pt-2"><strong> Work phone:</strong></h6><input required type="text" name="work_phone"  class="form-control" value="{{$estudianteBusca->work_phone}}"></div>
                            <div class="col-4 border-top border-left pb-1 pt-1"><h6 class="pb-3 pt-2"><strong> Cell phone:</strong></h6><input required type="text"  name="cell_phone" class="form-control" value="{{$estudianteBusca->cell_phone}}"></div>
                            <div class="w-100"></div>
                            <div class="col-6 border-top border-left pb-1 pt-1"><h6 class="pb-3 pt-2"><strong> Address:</strong></h6><input required type="text" name="address" class="form-control" value="{{$estudianteBusca->address}}"></div>
                            <div class="col-6 border-top border-left pb-1 pt-1"><h6 class="pb-3 pt-2"><strong> Email:</strong></h6><input required type="text" name="email" class="form-control" value="{{$estudianteBusca->email}}"></div>
                            <div class="w-100"></div>
                            <div class="col-6 border-top border-left pb-1"><h6 class="pb-3 pt-2"><strong> Student type:</strong></h6>
                                <select name="type_student" required id="type_student" class="custom-select select">
                                    <option value="{{$estudianteBusca->type_student}}" selected disabled>{{$estudianteBusca->type_student}}</option>
                                    <option value="Excelente">Excelente</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Irregular">Irregular</option>
                                </select>
                            </div>
                            @if ($estudianteBusca->id != 0)
                            <div class="col-2 border-top border-left pb-1 pt-5 text-right"><button type="submit" formaction="{{route('expedientes.limpiar', $estudianteBusca)}}" class="btn btn-warning botones"><strong>Clean</strong></button></div>
                            <div class="col-2 border-top border-left pb-1 pt-5 text-right"><button type="submit" formaction="{{route('expedientes.destroy', $estudianteBusca)}}" class="btn btn-danger botones"><strong>Delete</strong></button></div>
                            <div class="col-2 border-top border-left pb-1 pt-5 text-right"><button type="submit" formaction="{{route('expedientes.actualiza', $estudianteBusca)}}" class="btn btn-success botones"><strong>Update</strong></button></div>
                            @else
                            <div class="col-2 border-top border-left pb-1 pt-5 text-right"><button disabled class="btn btn-warning botones"><strong>Clean</strong></button></div>
                            <div class="col-2 border-top border-left pb-1 pt-5 text-right"><button disabled class="btn btn-danger botones"><strong>Delete</strong></button></div>
                            <div class="col-2 border-top border-left pb-1 pt-5 text-right"><button disabled class="btn btn-success botones"><strong>Update</strong></button></div>
                            @endif
                        </div>
                     </form>
                    </div>
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
