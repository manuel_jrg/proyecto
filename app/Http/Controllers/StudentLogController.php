<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentLogController extends Controller
{
    public function store(Request $request)
    {
        $estudiante = Student::create($request->all());
        $estudiantes = Student::all();
        $estudianteBusca = Student::all();
        $estudianteBusca = $estudianteBusca[0];
        $dat = 1;
        return view('home', compact(['estudiantes','estudianteBusca','dat']));
    }

    public function destroy($estudianteBusca)
    {
        $eliminar = Student::find($estudianteBusca);
        $eliminar->delete();

        $estudiantes = Student::all();
        $estudianteBusca = Student::all();
        $estudianteBusca = $estudianteBusca[0];
        $dat=2;
        return view('home', compact(['estudiantes','estudianteBusca','dat']));
    }

    public function actualiza(Request $request, Student $estudianteBusca)
    {
        // return $request;
        // return $estudianteBusca;
        $estudianteBusca->update($request->all());
        $estudiantes = Student::all();
        $dat = 3;
        return view('home', compact(['estudiantes','estudianteBusca','dat']));
    }
}
