<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $estudiantes = Student::all();
        $estudianteBusca = Student::all();
        $estudianteBusca = $estudianteBusca[0];
        return view('home', compact(['estudiantes','estudianteBusca']));
    }

    public function store(Request $request)
    {
        $estudiante = Student::create($request->all());
        $estudiantes = Student::all();
        $estudianteBusca = Student::all();
        $estudianteBusca = $estudianteBusca[0];
        $dat = 1;
        return view('home', compact(['estudiantes','estudianteBusca','dat']));
    }

    public function destroy($estudianteBusca)
    {
        $eliminar = Student::find($estudianteBusca);
        $eliminar->delete();

        $estudiantes = Student::all();
        $estudianteBusca = Student::all();
        $estudianteBusca = $estudianteBusca[0];
        $dat=2;
        return view('home', compact(['estudiantes','estudianteBusca','dat']));
    }

    public function actualiza(Request $request, Student $estudianteBusca)
    {
        $estudianteBusca->update($request->all());
        $estudiantes = Student::all();
        $dat = 3;
        return view('home', compact(['estudiantes','estudianteBusca','dat']));
    }
    public function expediente()
    {
        $estudiantes = Student::all();
        $estudianteBusca = Student::all();
        $estudianteBusca = $estudianteBusca[0];
        return view('home', compact(['estudiantes','estudianteBusca']));
    }

    public function buscar($id)
    {
        $estudiantes = Student::all();
        $estudianteBusca = Student::find($id);
        
        return view('home',compact('estudiantes','estudianteBusca'));
    }

    public function buscaID(Request $request)
    {

        $estudianteBusca = Student::find($request->id);
        if ($estudianteBusca == null) {
            $estudianteBusca = Student::all();
            $estudianteBusca = $estudianteBusca[0];
            $estudiantes = Student::all();
            $dat = 0;
            return view('home', compact(['estudiantes','estudianteBusca','dat']));
        }
        else
        {
            $estudiantes = Student::all();
            return view('home', compact(['estudiantes','estudianteBusca']));
        }
        
    }   

    public function limpiar($dato)
    {
        $estudianteBusca = Student::find($dato);
        $estudianteBusca->id = '';
        $estudianteBusca->document_id = '';
        $estudianteBusca->name = '';
        $estudianteBusca->last_name = '';
        $estudianteBusca->home_phone = '';
        $estudianteBusca->work_phone = '';
        $estudianteBusca->cell_phone = '';
        $estudianteBusca->address = '';
        $estudianteBusca->email = '';
        $estudianteBusca->type_student = '';
        
        $estudiantes = Student::all();
        
        return view('home',compact('estudiantes','estudianteBusca'));
    }
}
