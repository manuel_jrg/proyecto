<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast\Object_;

class ExpedienteController extends Controller
{
    public function expediente()
    {
        $estudiantes = Student::all();
        $estudianteBusca = Student::all();
        $estudianteBusca = $estudianteBusca[0];
        return view('home', compact(['estudiantes','estudianteBusca']));
    }

    public function buscar($id)
    {
        $estudiantes = Student::all();
        $estudianteBusca = Student::find($id);
        
        return view('home',compact('estudiantes','estudianteBusca'));
    }

    public function buscaID(Request $request)
    {

        $estudianteBusca = Student::find($request->id);
        if ($estudianteBusca == null) {
            $estudianteBusca = Student::all();
            $estudianteBusca = $estudianteBusca[0];
            $estudiantes = Student::all();
            $dat = 0;
            return view('home', compact(['estudiantes','estudianteBusca','dat']));
        }
        else
        {
            $estudiantes = Student::all();
            return view('home', compact(['estudiantes','estudianteBusca']));
        }
        
    }   

    public function limpiar($dato)
    {
        $estudianteBusca = Student::find($dato);
        $estudianteBusca->id = '';
        $estudianteBusca->document_id = '';
        $estudianteBusca->name = '';
        $estudianteBusca->last_name = '';
        $estudianteBusca->home_phone = '';
        $estudianteBusca->work_phone = '';
        $estudianteBusca->cell_phone = '';
        $estudianteBusca->address = '';
        $estudianteBusca->email = '';
        $estudianteBusca->type_student = '';
        
        $estudiantes = Student::all();
        
        return view('home',compact('estudiantes','estudianteBusca'));
    }
}
