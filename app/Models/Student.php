<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = ['document_id',
    'name',
    'last_name',
    'home_phone',
    'work_phone',
    'cell_phone',
    'address',
    'email',
    'type_student'];
}
